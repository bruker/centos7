#!/bin/sh
# curl https://gitlab.pasteur.fr/bruker/centos7/raw/master/ks/c7-sshd_config.sh | sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# sshd
/usr/bin/sed -i -e 's/^PermitRootLogin.*/PermitRootLogin no/g' /etc/ssh/sshd_config
/usr/bin/sed -i -e 's/^PasswordAuthentication.*/PasswordAuthentication no/g' /etc/ssh/sshd_config
#
# fix selinux permissions
/sbin/restorecon -rv /home /etc/ssh


