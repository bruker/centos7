#!/bin/sh
# curl https://gitlab.pasteur.fr/bruker/centos7/raw/master/ks/c7-add-tru.sh | sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
/sbin/useradd -u 2765 -g users -G wheel -m tru
(cd ~tru && /bin/curl https://gitlab.pasteur.fr/bruker/centos7/raw/master/ks/tru/c7-kickstart-minimal.tgz| tar xzvf -)
/bin/chown -R tru:users ~tru
/bin/chmod 700 ~tru
/sbin/restorecon -Rv ~tru
# sudoers
echo 'tru ALL = NOPASSWD: ALL' >> /etc/sudoers
echo coucou | /usr/bin/passwd --stdin tru
# fix selinux permissions
/sbin/restorecon -rv /home/tru /etc/sudoers


