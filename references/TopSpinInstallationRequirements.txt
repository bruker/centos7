                         CentOS 7 TopSpin Installation

Preface: We use the CentOS-7.1.1503 release with the KDE Environment

   Warning: The default kernel 3.10.0-123-*.el7 series in the older CentOS-7
   can't be used

 KB Item #11961 -- topspin does not start on computers with Linux kernel 3.10 - 3.13 due to:

   [1]https://bugzilla.redhat.com : Bug 1042807 - system call msgrcv() freom
   32-bit application shows error: msgrcv: No message of desired type
   [2]http://bugs.centos.org: Bug 007099: system call msgrcv() from 32-bit
   application shows error: msgrcv: No message of desired type

   Therefore we do not recommend CentOS-7.0 for TopSpin Installation.

   In the CentOS-7.1.1503 release this bug is fixed.

                           Installation Requirements

   TopSpin requires 32-Bit libraries, which are not installed automatically
   on 64-Bit OS since RHEL-6  / CentOS-6.

The recommended way to install the required TopSpin runtime environment
for CentOS-7.x86_64 is the Bruker-Addon Repository:

  Installation of Bruker-Addon Repository

     * yum --nogpgcheck install
       ftp://ftp.bruker.de/pub/nmr/CentOS/7/Bruker-Addon/bruker-addon-latest.x86_64.rpm

   Then you may install other required packages with yum (the bruker-repo is
   not enabled per default,
   you must add --enablerepo=bruker to the command line)

   The next step is to enable the EPEL repository, if not already done:

     * yum install epel-release

   Now you can install the TopSpin environment:

     * yum install --enablerepo=bruker   bruker-topspin-environ

   or

     * yum install --enablerepo=bruker   bruker-acquisition-environ

   If you open a new shell window the alias yumb is available as shortcut for
   'yum --enablerepo=bruker'

  [3]View Repository Infos

                              Installation Caveats

Default network assignment is not supported by the Flexlm Licence Manager

   The default network interface names are something like 'eno1' / 'ens1' or
   'enp0s3' , which are not detected by the flexlm library. 'lmhostid'
   returns an empty string.

   You may install the bruker-network-service which will create the required
   eth0 as IP link on each reboot. The link is set to the default device
   ens1.

   If you use or want a other device, e.g. eno1, * either copy and edit the
   config file /usr/lib/systemd/system/topspin-network.service and change
   --device=ens1 to --device=eno1

     * yum install --enablerepo=bruker  bruker-network-service

     * cp /usr/lib/systemd/system/topspin-network.service
       /etc/systemd/system/topspin-network.service

     * Edit /etc/systemd/system/topspin-network.service

     * systemctl enable topspin-network.service

     * systemctl start topspin-network.service

   * or reinstall FlexLM from the TopSpin DVD and specify eno1 when asked for
   a network interface name for the License Manager.

References

   Visible links
   1. https://bugzilla.redhat.com/show_bug.cgi?id=1042807
   2. http://bugs.centos.org/view.php?id=7099
   3. file:///home/tru/git/gitlab.pasteur.fr/bruker/centos7/Bruker-Addon/repoview/index.html
