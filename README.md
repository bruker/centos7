# Installation notes on Bruker Topsin installation on CentOS-7

reference: ftp://ftp.bruker.de/pub/nmr/CentOS/7/TopSpinInstallationRequirements.html (local copy retrived on 2017/04/21 in the reference folder)

local bruker for CentOS-7 mirror from the campus only: ftp://atsukau.bis.pasteur.fr/pub/bruker or http://atsukau.bis.pasteur.fr:81/pub/bruker

# kickstart notes

- using eth0/eth1 (legacy naming) instead of the "consistent naming"
(https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Networking_Guide/sec-Consistent_Network_Device_Naming_Using_biosdevname.html)

Install with the following kernel arguments:
```
"net.ifnames=0" "biosdevname=0" 
```

* default linux users for local access only (remote ssh with keys only)
 - root: bruker2017++
 - nmrsu: topspin
 - tru: coucou + ssh keys for this project

# this does not include topspin (you need to register on bruker website to download it)

# kickstart

example syslinux.conf/extlinux.conf
```
prompt 0
timeout 1
# prompt 1
#timeout 600
default linux
label linux
  kernel centos/7.3.1611/x86_64/vmlinuz
  append initrd=centos/7.3.1611/x86_64/initrd.img inst.stage2=http://192.168.56.1/pub/centos/7.3.1611/os/x86_64 ksdevice=link ip=dhcp noipv6 inst.ks=https://gitlab.pasteur.fr/bruker/centos7/raw/master/ks/C7.x86_64.bruker-http-sda-sdb.cfg net.ifnames=0 biosdevname=0
#  append initrd=centos/7.3.1611/x86_64/initrd.img inst.stage2=http://192.168.56.1/pub/centos/7.3.1611/os/x86_64 ksdevice=link ip=dhcp noipv6 inst.ks=https://gitlab.pasteur.fr/bruker/centos7/raw/master/ks/C7.x86_64.bruker-http-sda-sdb.cfg net.ifnames=0 biosdevname=0 inst.sshd=1 console=tty0 console=ttyS0,57600n1
```

- ks/C7.x86_64.bruker-http-sda.cfg (single disk /boot + lvm for /, /home, /opt and swap -> 54 GB minimal)
- ks/C7.x86_64.bruker-http-sda-sdb.cfg (/boot on sda1 (1GB) + / on partitionless sdb)

- 
